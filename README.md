# Data-driven Innovation Diffusion
**More details will be available here after the paper manuscript has been accepted.**
### The goal is to determine market intervention location on the census tracts level, for better diffusion of knowledge on the rooftop solar panels into the community.
### Data Used  
- U.S. Census
- New York State Residential Rooftop solar adoption rate.

#### Eaxample of different information spread pattern over time based on different ways of selecting intervention location:  
- left: highly influential tracts in Erie County, NY
- middle: census tract with the highest income level
- right: our proposed way, based on similarity zones

![](output.gif)

#### The figure below show the proposed way of selecting the intervention location produced a lower spatial asymmetry level measured by spatial auto-correlation.
![](./figs/rand_walk/moranIp_all_v2.png)
<!-- ![Comparison of different intervention locations](EPall2_inc_katz_inter.mp4) -->
<!-- blank line -->
<!-- <figure class="video_container">
  <iframe src="https://www.youtube.com/watch?v=BMsaHjfVORU&feature=youtu.be" frameborder="0"> </iframe>
</figure> -->
<!-- blank line -->
